<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$dbConfig = [
    'DB_NAME' => 'studentstestsdb',
    'DB_USER' => 'root',
    'DB_PASSWORD' => '',
    'DB_HOST' => 'localhost'
];

/** The name of the database for WordPress */
define('DB_NAME', $dbConfig['DB_NAME']);

/** MySQL database username */
define('DB_USER', $dbConfig['DB_USER']);

/** MySQL database password */
define('DB_PASSWORD', $dbConfig['DB_PASSWORD']);

/** MySQL hostname */
define('DB_HOST', $dbConfig['DB_HOST']);

require_once 'inc/model/mysql_pdo.php';

include_once 'inc/objects/Login.php';
include_once 'inc/objects/User.php';
include_once 'inc/objects/Admin.php';
include_once 'inc/objects/Student.php';
include_once 'inc/objects/Tests.php';

define("SHOW_ERRORS", 1);
if (SHOW_ERRORS) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}