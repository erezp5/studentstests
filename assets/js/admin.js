(() => {

    let addTestsWrp = document.getElementById('addTestsWrp');
    if (addTestsWrp) {
        let firstName = document.getElementById('firstName');
        let lastName = document.getElementById('lastName');
        let username = document.getElementById('username');
        let email = document.getElementById('email');

        let events = [firstName, lastName];
        for (let i = 0; i < events.length; i++) {
            events[i].addEventListener('keyup', (event) => {
                username.value = (firstName.value + '_' +  lastName.value).toLowerCase();
            });
        }

        let addTestSubmit = document.getElementById('addTestSubmit');
        addTestSubmit.addEventListener('click', (event) => {

        });
    }

    let testsTableBody = document.getElementById('testsTableBody');
    if (testsTableBody) {
        fetchTests();
    }

    let doSearch = document.getElementById('doSearch');
    let searchInput = document.getElementById('searchInput');
    if (doSearch && searchInput) {
        doSearch.addEventListener('click', (event) => {
            let s = searchInput.value;
            fetchTests('', 'asc', s);
        });
        searchInput.addEventListener('keydown', (event) => {
            if (event.key === 'Enter') {
                let s = searchInput.value;
                fetchTests('', 'asc', s);
            }
        });
    }


    let sortByID = document.getElementById('sortByID');
    if (sortByID) {
        sortByID.addEventListener('click', (event) => {
            sortBy(sortByID, 'id');
        });
    }
    let orderByTitle = document.getElementById('orderByTitle');
    if (orderByTitle) {
        orderByTitle.addEventListener('click', (event) => {
            sortBy(orderByTitle, 'title');
        });
    }
    let orderByYear = document.getElementById('orderByYear');
    if (orderByYear) {
        orderByYear.addEventListener('click', (event) => {
            sortBy(orderByYear, 'year');
        });
    }
    let orderBySemester = document.getElementById('orderBySemester');
    if (orderBySemester) {
        orderBySemester.addEventListener('click', (event) => {
            sortBy(orderBySemester, 'semester');
        });
    }

})();

function sortBy(el, orderBy) {
    let asc = (el.querySelector('i').classList.contains('fa-sort-asc'));
    let searchInput = document.getElementById('searchInput');
    clearDescAsc();
    if (asc) {
        fetchTests(orderBy, 'desc', searchInput.value);
        el.querySelector('i').classList.remove('fa-sort-asc');
        el.querySelector('i').classList.add('fa-sort-desc');
    } else {
        fetchTests(orderBy, 'asc', searchInput.value);
        el.querySelector('i').classList.remove('fa-sort-desc');
        el.querySelector('i').classList.add('fa-sort-asc');
    }
}

function clearDescAsc() {
    let el = document.getElementsByClassName('fa-sort');
    for(let i = 0; i < el.length; i++) {
        el[i].classList.remove('fa-sort-asc','fa-sort-desc');
    }
}

function fetchTests(orderBy = '', order = 'asc', s = '') {
    let url = 'actions.php';
    fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                action: 'getTests',
                orderBy: orderBy,
                order: order,
                s: s
            })
        })
        .then(
            (response) => {
                return response.json();
            })
        .then(
            (jsonObject) => {
                setTestsTable(jsonObject);
            });
}

function setTestsTable(json) {
    let testsTableBody = document.getElementById('testsTableBody');
    testsTableBody.innerHTML = '';
    json.forEach((v, k) => {
        let tr = document.createElement('tr');
        let td_id = document.createElement('td');
        td_id.innerHTML = v.id;
        tr.appendChild(td_id);
        let td_title = document.createElement('td');
        td_title.innerHTML = v.title;
        tr.appendChild(td_title);
        let td_year = document.createElement('td');
        td_year.innerHTML = v.year;
        tr.appendChild(td_year);
        let td_semester = document.createElement('td');
        td_semester.innerHTML = v.semester;
        tr.appendChild(td_semester);
        testsTableBody.appendChild(tr);
    });
}