(() => {

    fetchTests();

    let doSearch = document.getElementById('doSearch');
    let searchInput = document.getElementById('searchInput');
    doSearch.addEventListener('click', (event) => {
        let s = searchInput.value;
        fetchTests('', 'asc', s);
    });
    searchInput.addEventListener('keydown', (event) => {
        if (event.key === 'Enter') {
            let s = searchInput.value;
            fetchTests('', 'asc', s);
        }
    });


    let sortByID = document.getElementById('sortByID');
    sortByID.addEventListener('click', (event) => {
        sortBy(sortByID, 'test_ID');
    });
    let orderByTitle = document.getElementById('orderByTitle');
    orderByTitle.addEventListener('click', (event) => {
        sortBy(orderByTitle, 'title');
    });
    let orderByYear = document.getElementById('orderByYear');
    orderByYear.addEventListener('click', (event) => {
        sortBy(orderByYear, 'year');
    });
    let orderBySemester = document.getElementById('orderBySemester');
    orderBySemester.addEventListener('click', (event) => {
        sortBy(orderBySemester, 'semester');
    });
    let orderByTestScore = document.getElementById('orderByTestScore');
    orderByTestScore.addEventListener('click', (event) => {
        sortBy(orderByTestScore, 'test_score');
    });

})();


function sortBy(el, orderBy) {
    let asc = (el.querySelector('i').classList.contains('fa-sort-asc'));
    let searchInput = document.getElementById('searchInput');
    clearDescAsc();
    if (asc) {
        fetchTests(orderBy, 'desc', searchInput.value);
        el.querySelector('i').classList.remove('fa-sort-asc');
        el.querySelector('i').classList.add('fa-sort-desc');
    } else {
        fetchTests(orderBy, 'asc', searchInput.value);
        el.querySelector('i').classList.remove('fa-sort-desc');
        el.querySelector('i').classList.add('fa-sort-asc');
    }
}

function clearDescAsc() {
    let el = document.getElementsByClassName('fa-sort');
    for(let i = 0; i < el.length; i++) {
        el[i].classList.remove('fa-sort-asc','fa-sort-desc');
    }
}

function fetchTests(orderBy = '', order = 'asc', s = '') {
    let url = 'actions.php';
    fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            action: 'getTestsByUserId',
            orderBy: orderBy,
            order: order,
            s: s
        })
    })
    .then(
        (response) => {
            return response.json();
        })
    .then(
        (jsonObject) => {
            setTestsTable(jsonObject);
        });
}

function setTestsTable(json) {
    let testsTableBody = document.getElementById('testsTableBody');
    testsTableBody.innerHTML = '';
    json.forEach((v, k) => {
        let tr = document.createElement('tr');
        let td_id = document.createElement('td');
        td_id.innerHTML = v.test_ID;
        tr.appendChild(td_id);
        let td_title = document.createElement('td');
        td_title.innerHTML = v.title;
        tr.appendChild(td_title);
        let td_year = document.createElement('td');
        td_year.innerHTML = v.year;
        tr.appendChild(td_year);
        let td_semester = document.createElement('td');
        td_semester.innerHTML = v.semester;
        tr.appendChild(td_semester);
        let td_test_score = document.createElement('td');
        td_test_score.innerHTML = v.test_score;
        tr.appendChild(td_test_score);
        testsTableBody.appendChild(tr);
    });
}