<?php

require_once 'config.php';
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if ($contentType === "application/json") {
    $content = trim(file_get_contents("php://input"));

    $decoded = json_decode($content, true);

    if(is_array($decoded)) {
        $action = $decoded['action'];
    } else {
        echo json_encode(['error' => 'cant resolve json']);
    }
}

function testsOrderBy($orderBy, $order) {
	$return = "ORDER BY ";
	if (isset($orderBy) && $orderBy !== '') {
		switch ($orderBy) {
			case 'title':
			case 'year':
			case 'semester':
				$return .= "t.".$orderBy;
				break;
			case 'test_score':
			case 'test_ID':
				$return .= "st.".$orderBy;
				break;
			default:
				$return .= "t.id";
				break;
		}
	} else {
		$return .= "t.id";
	}

	if (isset($order) && $order === 'desc') {
		$return .= " ".$order;
	} else {
		$return .= " asc";
	}

	return $return;
}

function testsSearch($s = '', $first = false) {
	$search = '';
    if (isset($s) && $s !== '') {
	    $search = ($first) ? '' : ' AND ';
		$search .= "t.title LIKE '%".$s."%'";
	}
	return $search;
}

function parseStudentsTests($result) {
	$return = [];
	foreach ($result as $k => $v) {
		$return[] = [
			'test_ID' => $v['test_ID'],
			'title' => $v['title'],
			'year' => $v['year'],
			'semester' => $v['semester'],
			'test_score' => $v['test_score']
		];
	}
	return $return;
}

function parseTests($result) {
	$return = [];
	foreach ($result as $k => $v) {
		$return[] = [
			'id' => $v['id'],
			'title' => $v['title'],
			'year' => $v['year'],
			'semester' => $v['semester'],
		];
	}
	return $return;
}

switch ($action) {
    case 'getTestsByUserId':
        $user_id = $_SESSION['user'];
        if ($user_id) {
            $db = DB::instance();

            $order = testsOrderBy($decoded['orderBy'], $decoded['order']);
            $search = testsSearch($decoded['s']);

            $sql = /** @lang MySQL */
                "SELECT *
             FROM tests as t
             INNER JOIN studetns_tests as st
             ON t.id = st.test_ID
			 WHERE st.user_ID = $user_id $search
			 $order";

            $result = $db->query($sql);
            if (!empty($result)) {
                $return = parseStudentsTests($result);
                echo json_encode($return);
            } else {
                echo json_encode(['error' => 'No tests for user']);
            }
        } else {
            echo json_encode(['error' => 'No user logged in']);
        }
        break;

	case 'getTests':
		$user_id = $_SESSION['user'];
		$user_role = $_SESSION['userRole'];
		if ($user_role === 'admin') {
			$db = DB::instance();

			$order = testsOrderBy($decoded['orderBy'], $decoded['order']);
			$search = testsSearch($decoded['s'], true);
			$where = ($search) ? "WHERE" . $search : '';

			$sql = /** @lang MySQL */
				"SELECT *
	             FROM tests as t
				 $where $order";

			$result = $db->query($sql);
			if (!empty($result)) {
				$return = parseTests($result);
				echo json_encode($return);
			} else {
				echo json_encode(['error' => 'No tests ']);
			}
		} else {
			echo json_encode(['error' => 'No admin logged in']);
		}
		break;

    case 'addTest':

        break;
    default:
        exit();
        break;
}
