-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2019 at 09:22 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentstestsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `studetns_tests`
--

DROP TABLE IF EXISTS `studetns_tests`;
CREATE TABLE `studetns_tests` (
  `user_ID` int(11) NOT NULL,
  `test_ID` int(11) NOT NULL,
  `test_score` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studetns_tests`
--

INSERT INTO `studetns_tests` (`user_ID`, `test_ID`, `test_score`) VALUES
(2, 1, 95),
(2, 2, 89),
(2, 3, 85),
(2, 4, 92),
(2, 5, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `year` year(4) NOT NULL,
  `semester` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `title`, `year`, `semester`) VALUES
(1, 'Front End Developer', 2018, 1),
(2, 'Java A', 2018, 1),
(3, 'Java B', 2018, 2),
(4, 'PHP OOP', 2018, 2),
(5, 'CSS3', 2019, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userName` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL,
  `lastName` varchar(32) NOT NULL,
  `firstName` varchar(32) NOT NULL,
  `email` varchar(256) NOT NULL,
  `role` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userName`, `password`, `lastName`, `firstName`, `email`, `role`) VALUES
(1, 'admin', '7cbb3252ba6b7e9c422fac5334d22054', '', '', 'admin@admin.com', 'admin'),
(2, 'moran_cohen', '7cbb3252ba6b7e9c422fac5334d22054', 'Cohen', 'Moran', 'moran_cohen@walla.com', 'student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `studetns_tests`
--
ALTER TABLE `studetns_tests`
  ADD PRIMARY KEY (`user_ID`,`test_ID`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
