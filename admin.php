<?php

require_once 'config.php';
$login = new Login();
if ($login->isLogin() === true && $login->getRole() === 'admin') {
    $admin = new Admin(clone $login);
} else {
    header("Location: login.php");
}

include_once 'header.php';
?>

<body class="text-center">

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	<a class="navbar-brand" href="#" >Some College</a>

	<div class="collapse navbar-collapse" >
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tests</a>
				<div class="dropdown-menu" aria-labelledby="dropdown01">
					<a class="dropdown-item" href="admin.php">Tests</a>
					<a class="dropdown-item" href="admin.php?action=addTest">Add Test</a>

				</div>
			</li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Users</a>
                <div class="dropdown-menu" aria-labelledby="dropdown02">
                    <a class="dropdown-item" href="admin.php?action=allUsers">Users</a>
                    <a class="dropdown-item" href="admin.php?action=addUser">Add User</a>

                </div>
            </li>
		</ul>
		<div class="form-inline my-2 my-lg-0">
			<span style="color: #fff">Hello <?=$admin->getUserName()?>, <a href="login.php?logout=true">Log Out</a> </span>
		</div>
	</div>
</nav>

<div class="container">

    <?php
    $action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'allTests';
    switch ($action) {
        case 'allTests':
        case 'addTest':
        case 'allUsers':
        case 'addUser':
            include "inc/templates/{$action}.php";
            break;
        default:
            include "inc/templates/allTests.php";
            break;
    }
    ?>

</div>

<script type="text/javascript" src="./assets/js/admin.js"></script>
<?php include_once 'footer.php'; ?>

