<?php

require_once 'config.php';
$login = new Login();
if ($login->isLogin() === true) {
    if ($login->getRole() === 'admin') {
        header("Location: admin.php");
    } else if ($login->getRole() === 'student') {
        header("Location: tests.php");
    }
} else if (isset($_REQUEST['logout'])) {
	header("Location: login.php");
}

include_once 'header.php';
?>

    <body class="text-center">
    <form class="form-signin" action="" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="userName" class="sr-only">User Name</label>
        <input type="text" name="userName" id="userName" class="form-control" placeholder="User Name" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

<?php include_once 'footer.php'; ?>
