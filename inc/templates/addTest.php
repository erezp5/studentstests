<?php ?>
<div id="addTestsWrp" class="row">
    <div class="col-md-8 order-md-1">
        <h4 class="mb-3">Add test</h4>
        <form >

            <div class="mb-3">
                <label for="testTitle">Test Title</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="testTitle" placeholder="Test Title" >
                </div>
            </div>

            <div class="mb-3">
                <label for="year">Year</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="year" placeholder="Year" >
                </div>
            </div>

            <div class="mb-3">
                <label for="semester">Semester</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="semester" placeholder="Semester" >
                </div>
            </div>

            <button class="btn btn-primary btn-lg btn-block" id="addUserSubmit" >Send</button>
        </form>
    </div>

</div>