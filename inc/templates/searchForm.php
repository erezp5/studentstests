<?php ?>

<div class="row">
    <label for="searchInput" class="sr-only">Search</label>
    <input type="text" id="searchInput" placeholder="Search..." />
    <button id="doSearch">Search</button>
</div>