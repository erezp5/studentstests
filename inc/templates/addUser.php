<?php ?>
<div id="addUserWrp" class="row">
    <div class="col-md-8 order-md-1">
        <h4 class="mb-3">Add User</h4>
        <form >
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="firstName">First name</label>
                    <input type="text" class="form-control" id="firstName" placeholder="" value="" >
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName">Last name</label>
                    <input type="text" class="form-control" id="lastName" placeholder="" value="" >
                </div>
            </div>

            <div class="mb-3">
                <label for="username">Username</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="username" placeholder="Username" >
                </div>
            </div>

            <div class="mb-3">
                <label for="email">Email </label>
                <input type="email" class="form-control" id="email" placeholder="you@example.com">
            </div>

            <button class="btn btn-primary btn-lg btn-block" id="addTestSubmit" >Send</button>
        </form>
    </div>

</div>