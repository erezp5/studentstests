<?php ?>

<table class="table">
    <thead id="usersTableHead">
    <tr>
        <th id="sortByID" scope="col">ID <i class="fa fa-fw fa-sort fa-sort-asc"></i></th>
        <th id="orderByTitle" scope="col">Title <i class="fa fa-fw fa-sort"></i></th>
        <th id="orderByYear" scope="col">Year <i class="fa fa-fw fa-sort"></i></th>
        <th id="orderBySemester" scope="col">Semester <i class="fa fa-fw fa-sort"></i></th>
    </tr>
    </thead>
    <tbody id="usersTableBody">

    </tbody>
</table>
