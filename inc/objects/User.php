<?php


class User {
    private $id;
    private $userName;
    private $lastName;
    private $firstName;
    private $email;
    private $role;

    public function __construct($login) {

        $this->id = $login->getId();
        $this->userName = $login->getUserName();
        $this->lastName = $login->getLastName();
        $this->firstName = $login->getFirstName();
        $this->email = $login->getEmail();
        $this->role = $login->getRole();
    }

    public function getId() {
        return $this->id;
    }
	public function getUserName() {
		return $this->userName;
	}
	public function getLastName() {
		return $this->lastName;
	}
	public function getFirstName() {
		return $this->firstName;
	}
	public function getEmail() {
		return $this->email;
	}
}