<?php

class Login {

    private $id;
    private $userName;
    private $password;
    private $lastName;
    private $firstName;
    private $email;
    private $isLogin = false;
    private $role;

    public function __construct() {

	    if (isset($_REQUEST['logout'])) {
		    $this->logOut();
	    } else if (isset($_POST['userName']) && isset($_POST['password'])) {
            $this->userName = $_POST['userName'];
            $this->password = md5($_POST['password']);
            $this->loginByUserName();
        } else if (isset($_SESSION['user']) ) {
            $this->id = $_SESSION['user'];
            $this->loginByUserId();
        }

    }

    public function isLogin() {
        return $this->isLogin;
    }
    public function getRole() {
        return $this->role;
    }
    public function getId() {
        return $this->id;
    }
    public function getUserName() {
        return $this->userName;
    }
    public function getLastName() {
        return $this->lastName;
    }
    public function getFirstName() {
        return $this->firstName;
    }
    public function getEmail() {
        return $this->email;
    }

	private function logOut() {
		$this->isLogin = false;
		session_unset();
		session_destroy();
	}

    private function loginByUserName() {
        $db = DB::instance();

        $sql = /** @lang MySQL */
                "SELECT *
                FROM `users`
				WHERE userName = '$this->userName' AND `password` = '$this->password'";

        $result = $db->query($sql);
        if (isset($result[0]) && isset($result[0]['id'])) {
            $this->parseUser($result);
        }
    }

    private function loginByUserId() {
        $db = DB::instance();

        $sql = /** @lang MySQL */
                "SELECT *
                FROM `users`
				WHERE id = $this->id";

        $result = $db->query($sql);
        if (isset($result[0]) && isset($result[0]['id'])) {
            $this->parseUser($result);
        }
    }

    private function parseUser($result) {
        $this->isLogin = true;
        $_SESSION['user'] = $result[0]['id'];
        $_SESSION['userRole'] = $result[0]['role'];
        $this->id = $result[0]['id'];
        $this->userName = $result[0]['userName'];
        $this->firstName = $result[0]['firstName'];
        $this->lastName = $result[0]['lastName'];
        $this->email = $result[0]['email'];
        $this->role = $result[0]['role'];
    }

}