<?php

Class SafePDO extends PDO {

    /**
     * @param $exception Exception
     */
    public static function exception_handler($exception) {

        // Output the exception details
        die('Uncaught exception: '. $exception->getMessage());
    }

    public function __construct($dsn, $username='', $password='', $driver_options=array()) {

        // Temporarily change the PHP exception handler while we . . .
        set_exception_handler(array(__CLASS__, 'exception_handler'));

        // . . . create a PDO object
        parent::__construct($dsn, $username, $password, $driver_options);

        // Change the exception handler back to whatever it was before
        restore_exception_handler();
    }
}

class DB {

    /**
     * @var PDO
     */
    private static $conn;

    /**
     * DB constructor.
     */
    public function __construct() {
        $this->CreateCon();
    }

    private static function CreateCon() {
        try {
            $options = array();
            /*$options = array(
                PDO::ATTR_EMULATE_PREPARES => false,
                "ConnectionPooling"=>1,
                "MultiSubnetFailover" => 'yes',
                "Failover_Partner" => DB_FAILOVER
            );*/

            self::$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME . ";charset=utf8", DB_USER, DB_PASSWORD,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            //self::$conn->setAttribute(constant('PDO::SQLSRV_ATTR_DIRECT_QUERY'), true);

        } catch (PDOException $ex) {
            //show exception
            self::error($ex->getMessage());
        }
    }

    private static function error($msg) {
        header('HTTP/1.1 500 Internal Server Error');
        if (SHOW_ERRORS) {
            echo "Error in statement execution.\n";
            die(print_r($msg, true));
        }
    }

    public function __destruct() {
        $this->closeConn();
    }

    public function closeConn() {
        self::$conn = null;
    }

    public static function instance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new DB();
        }
        return $inst;
    }

    public static function ms_escape_string($data) {
        if (!isset($data) or empty($data)) return "''";
        if (is_numeric($data)) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ($non_displayables as $regex)
            $data = preg_replace($regex, '', $data);
        $data = str_replace("'", "''", $data);
        return "'$data'";
    }

    public function query($sql) {

        /* Execute the query. */

        $query_res = self::$conn->query($sql);

        if (!$query_res) {
            self::error("error on query :   <br> $sql");
        }

        $res = [];
        foreach ($query_res as $row) {
            array_push($res, $row);
        }

        return $res;
    }

    public function get($tblName, $filter = '1 = 1', $get = '*') {
        $sql = "SELECT $get FROM $tblName WHERE $filter";
        return self::query($sql);

    }

    public static function multiQuery($sql) {

        /* Execute the query. */
        $query_res = self::$conn->prepare($sql);
        $query_res->execute();

        if (!$query_res) {
            self::error('error');
        }

        $res = [];
        do {
            $row = $query_res->fetchAll(PDO::FETCH_ASSOC);
            array_push($res, $row);
        } while ($query_res->nextRowset());

        return $res;
    }
}
