<?php

require_once 'config.php';
$login = new Login();
if ($login->isLogin() === true && $login->getRole() === 'student') {
    $student = new Student(clone $login);
} else {
    header("Location: login.php");
}

include_once 'header.php';
?>

<body class="text-center">

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#" >Some College</a>


    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">My tests <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <span style="color: #fff">Hello <?=$student->getFirstName()?> <?=$student->getLastName()?>, <a href="login.php?logout=true">Log Out</a> </span>
        </div>
    </div>
</nav>

<div class="container">
<?php include 'inc/templates/searchForm.php'; ?>
<div class="row">
<table class="table">
    <thead id="testsTableHead">
    <tr>
        <th id="sortByID" scope="col">ID <i class="fa fa-fw fa-sort fa-sort-asc"></i></th>
        <th id="orderByTitle" scope="col">Title <i class="fa fa-fw fa-sort"></i></th>
        <th id="orderByYear" scope="col">Year <i class="fa fa-fw fa-sort"></i></th>
        <th id="orderBySemester" scope="col">Semester <i class="fa fa-fw fa-sort"></i></th>
        <th id="orderByTestScore" scope="col">Test Score <i class="fa fa-fw fa-sort"></i></th>
    </tr>
    </thead>
    <tbody id="testsTableBody">

    </tbody>
</table>
</div>
</div>

<script type="text/javascript" src="./assets/js/students.js"></script>
<?php include_once 'footer.php'; ?>

